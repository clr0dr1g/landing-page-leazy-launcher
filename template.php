<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Célio Rodrigues - Leazy Launcher Team">
	<title>Leazy Launcher</title>
	<link href="/home/css/bootstrap.min.css" rel="stylesheet">
	<link href="/home/css/grayscale.css" rel="stylesheet">
	<link href="/home/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-69239164-2', 'auto');
		ga('send', 'pageview');
	</script>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top">
					<i class="fa fa-play-circle"></i>  <span class="light">Leazy</span> Launcher
				</a>
			</div>

			<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
				<ul class="nav navbar-nav">
					<li class="hidden">
						<a href="#page-top"></a>
					</li>
					<li>
						<a class="page-scroll" href="/api"><?=_('menu.api')?></a>
					</li>
					<li>
						<a class="page-scroll" href="/comunity"><?=_('menu.comunity')?></a>
					</li>
					<li>
						<a class="page-scroll" href="#team"><?=_('menu.team')?></a>
					</li>
					<li>
						<a class="page-scroll" href="#about"><?=_('menu.about')?></a>
					</li>
					<li>
						<a class="page-scroll" href="#download"><?=_('menu.download')?></a>
					</li>
					<li>
						<a class="page-scroll" href="#contact"><?=_('menu.contact')?></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<header class="intro">
		<div class="intro-body">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1 class="brand-heading">Leazy Launcher</h1>
						<p class="intro-text"><?=_("home.slogan")?></p>
						<a href="#about" class="btn btn-circle page-scroll">
							<i class="fa fa-angle-double-down animated"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</header>

	<section id="about" class="container content-section text-center">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<h2><?=_("about.title")?></h2>
				<?=_("about.text")?>
			</div>
		</div>
	</section>

	<section id="download" class="content-section text-center">
		<div class="download-section">
			<div class="container">
				<div class="col-lg-8 col-lg-offset-2">
					<h2><?=_("download.title")?></h2>
					<p><?=_("download.text")?></p>
					<a href="/builds/latest-build" class="btn btn-default btn-lg" target="_blank"><?=_("download.link")?></a>
				</div>
			</div>
		</div>
	</section>

	<section id="contact" class="container content-section text-center">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<h2><?=_("contact.title")?></h2>
				<p><?=_("contact.text")?></p>
				<p><a href="mailto:contact@leazy.xyz">contact@leazy.xyz</a>
				</p>
				<ul class="list-inline banner-social-buttons">
					<li>
						<a href="https://twitter.com" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
					</li>
					<li>
						<a href="https://github.com/LeazyLauncher" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
					</li>
					<li>
						<a href="https://plus.google.com" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>
					</li>
				</ul>
			</div>
		</div>
	</section>

	<footer>
		<div class="container text-center">
			<p>Copyright &copy; Leazy <?=date('Y')?></p>
		</div>
	</footer>

	<script src="/home/js/jquery.js"></script>
	<script src="/home/js/bootstrap.min.js"></script>
	<script src="/home/js/jquery.easing.min.js"></script>
	<script src="/home/js/grayscale.js"></script>

</body>

</html>
