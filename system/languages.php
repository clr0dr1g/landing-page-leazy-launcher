<?php

	$langs = [

		"pt" => [
			"menu.api" => "Documentação",
			"menu.comunity" => "Comunidade",
			"menu.team" => "Time",
			"menu.about" => "Sobre",
			"menu.download" => "Baixar",
			"menu.contact" => "Contato",
			"home.slogan" => "Um tema Bootstrap livre. <br> Criado por Start Bootstrap.",
			"about.title" => "Sobre a Leazy Launcher",
			"about.text" => '<p>Tons de cinza é livre Bootstrap 3 tema criado por Iniciar Bootstrap. Ele pode ser seu agora mesmo, basta fazer o download do modelo no <a href="http://startbootstrap.com/template-overviews/grayscale/">a página de visualização</a>. O tema é de código aberto, e você pode usá-lo para qualquer fim, comercial ou pessoal.</p><p>Este tema apresenta fotos por <a href="http://gratisography.com/">Gratisography</a> juntamente com uma cortesia pele feita sob encomenda do Google Maps <a href="http://snazzymaps.com/">Snazzy Maps</a>.</p><p>Grayscale inclui HTML completa, CSS e arquivos JavaScript personalizados, juntamente com arquivos menos para a personalização fácil.</p>',
			"download.title" => "Baixar a Leazy Launcher",
			"download.text" => "Baixe gratuitamente Leazy Launcher e começar agora a experimentar e criar coisas novas.",
			"download.link" => "Clique para baixar",
			"contact.title" => "Contato Leazy",
			"contact.text" => "Sinta-se livre para enviar-nos para fornecer algum feedback sobre o nosso app, dá-nos sugestões para novas funcionalidades e temas, ou só para dizer Olá!"
		],

		"en" => [
			"menu.api" => "API Reference",
			"menu.comunity" => "Comunity",
			"menu.team" => "Team",
			"menu.about" => "About",
			"menu.download" => "Download",
			"menu.contact" => "Contact",
			"home.slogan" => "A free, responsive, one page Bootstrap theme.<br>Created by Start Bootstrap.",
			"about.title" => "About Leazy Launcher",
			"about.text" => '<p>Grayscale is a free Bootstrap 3 theme created by Start Bootstrap. It can be yours right now, simply download the template on <a href="http://startbootstrap.com/template-overviews/grayscale/">the preview page</a>. The theme is open source, and you can use it for any purpose, personal or commercial.</p><p>This theme features stock photos by <a href="http://gratisography.com/">Gratisography</a> along with a custom Google Maps skin courtesy of <a href="http://snazzymaps.com/">Snazzy Maps</a>.</p><p>Grayscale includes full HTML, CSS, and custom JavaScript files along with LESS files for easy customization.</p>',
			"download.title" => "Download Leazy",
			"download.text" => "Download free Leazy Launcher and start right now to try and create new things.",
			"download.link" => "Click to Download",
			"contact.title" => "Contact Leazy",
			"contact.text" => "Feel free to email us to provide some feedback on our app, give us suggestions for new features and themes, or to just say hello!"
		]

	];