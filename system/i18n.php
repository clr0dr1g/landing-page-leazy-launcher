<?php
	
	class i18n{

		private $params = array();
		private $currentLang = null;

		function __construct(){}

		public function setCurrentLanguage($lang){
			$this->currentLang = $lang;
		}

		public function add($params){
			$this->params = $params;
		}

		public function get($param, $debug){
			$inLang = @$this->params[$this->currentLang];
			if($inLang){
				$param = @$inLang[$param];
				if(!$param && $debug != false)
					return "Error param not found";
				else
					return $param;
			}else{
				return "Error language not found";
			}
		}
	}