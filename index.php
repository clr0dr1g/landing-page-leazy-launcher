<?php

	include("system/i18n.php");
	include("system/languages.php");

	$lang = (isset($_GET['lang']) && $_GET['lang'] != "") ? $_GET['lang'] : 'pt';
	

	$i18n = new i18n;
	
	$i18n->add($langs);
	$i18n->setCurrentLanguage($lang);
	
	function _($param, $debug = false){
		global $i18n;
		return $i18n->get($param, $debug);
	}

    include("template.php");